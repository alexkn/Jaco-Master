# Small updates but mostly copied from:
# https://github.com/facebook/duckling/blob/master/Dockerfile
# https://github.com/RasaHQ/duckling/blob/master/Dockerfile

FROM docker.io/haskell:8 AS builder

RUN apt-get update -qq && \
  apt-get install -qq -y libssl-dev libpcre3 libpcre3-dev build-essential git --fix-missing --no-install-recommends && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir /log

RUN git clone https://github.com/facebook/duckling
WORKDIR /duckling/

ENV LANG=C.UTF-8
RUN stack setup

RUN apt-get update && apt-get install -y --no-install-recommends pkg-config

# NOTE:`stack build` will use as many cores as are available to build
# in parallel. However, this can cause OOM issues as the linking step
# in GHC can be expensive. If the build fails, try specifying the
# '-j1' flag to force the build to run sequentially.
RUN stack install

# ==================================================================================================
# ==================================================================================================

FROM docker.io/debian:buster

ENV LANG C.UTF-8

RUN apt-get update -qq && \
  apt-get install -qq -y libpcre3 libgmp10 --no-install-recommends && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --from=builder /root/.local/bin/duckling-example-exe /usr/local/bin/

# Use port 8008 instead of default port, because port 8000 is already in use by portainer
EXPOSE 8008

CMD ["duckling-example-exe", "-p", "8008", "--no-access-log", "--no-error-log"]
