import time

import numpy as np
import webrtcvad

# ==================================================================================================


vad_model: webrtcvad.Vad
sample_rate = 16000

running_transcription = False
transcription_timeout = 0
timeout_counter = 0
min_time = 2.5
vad_buffer_size = int(sample_rate * 0.030)

# Collects the streamed audio
audio_buffer = np.array([], dtype=np.int16)

# Stores frames until they are processed, required if transcription is slower than frame feeding
cache_buffer = np.array([], dtype=np.int16)

# This buffer defines, how many (silent) frames are fed to DeepSpeech as padding at start
# There were some issues suggesting this improves detection accuracy
pre_buffer_size = int(sample_rate * 0.400)
pre_buffer = np.array([], dtype=np.int16)

# This buffer keeps (silent) frames for padding at the end
# If end_buffer_size is greater than pre_buffer_size only pre_buffer_size frames are padded
# The rest of the buffer is to give the speakers more time to think about the next words
end_buffer_size = int(sample_rate * 0.900)
end_buffer = np.array([], dtype=np.int16)

if pre_buffer_size < vad_buffer_size or end_buffer_size < vad_buffer_size:
    raise ValueError


# ==================================================================================================


def init(vad_aggressiveness, timeout_transcription):
    global vad_model, transcription_timeout

    vad_model = webrtcvad.Vad(vad_aggressiveness)
    transcription_timeout = timeout_transcription


# ==================================================================================================


def add_to_buffer(frames):
    global cache_buffer, pre_buffer, end_buffer

    frames = np.array(frames, dtype=np.int16)
    cache_buffer = np.append(cache_buffer, frames)

    if not running_transcription:
        if len(cache_buffer) > vad_buffer_size:
            # Don't store incoming frames if no transcription is running,
            # but keep some of them in cache, that we can detect start of speech automatically
            cache_buffer = cache_buffer[-vad_buffer_size:]

        pre_buffer = np.append(pre_buffer, frames)
        if len(pre_buffer) > pre_buffer_size:
            pre_buffer = pre_buffer[-pre_buffer_size:]


# ==================================================================================================


def empty_buffers():
    global cache_buffer, pre_buffer, end_buffer, audio_buffer

    cache_buffer = np.array([], dtype=np.int16)
    pre_buffer = np.array([], dtype=np.int16)
    end_buffer = np.array([], dtype=np.int16)
    audio_buffer = np.array([], dtype=np.int16)


# ==================================================================================================


def contains_speech(vad: webrtcvad.Vad, buffer) -> bool:
    """Use voice activity detection to check if someone is currently speaking"""
    frame = np.array(buffer, dtype=np.int16).tostring()
    is_speech = vad.is_speech(frame, sample_rate)
    return is_speech


# ==================================================================================================


def start_transcription():
    global running_transcription, audio_buffer, timeout_counter

    running_transcription = True
    timeout_counter = time.time()
    print("Started transcription")
    # Feed the last (silent) frames as padding at start
    audio_buffer = np.append(audio_buffer, pre_buffer)


# ==================================================================================================


def end_transcription(last_buffer: np.array) -> str:
    global running_transcription, timeout_counter

    running_transcription = False
    timeout_counter = 0
    audio = np.append(audio_buffer, last_buffer)

    empty_buffers()
    return audio


# ==================================================================================================


def run_slu_step():
    global running_transcription, audio_buffer, cache_buffer, end_buffer, timeout_counter, vad_model

    if len(cache_buffer) < vad_buffer_size:
        # Nothing to do yet, wait for more data
        return None

    vad_buffer = cache_buffer[:vad_buffer_size]
    cache_buffer = cache_buffer[vad_buffer_size:]

    if timeout_counter != 0 and time.time() - timeout_counter > transcription_timeout:
        # Return text detected until timeout
        text = end_transcription(vad_buffer)
        print("Stopped transcription due to timeout")
        return text

    if (
        running_transcription
        and len(end_buffer) >= end_buffer_size
        and timeout_counter != 0
        and time.time() - timeout_counter > min_time
    ):
        # No speech was detected for end_buffer_size frames, we also listened for min_time secs
        # -> End the transcription now
        feed_buffer = end_buffer[:pre_buffer_size]
        text = end_transcription(feed_buffer)
        return text

    is_speech = contains_speech(vad_model, vad_buffer)
    if is_speech is True:
        if not running_transcription:
            start_transcription()
            # Return here, that we don't add the current frames, they are already in the pre_buffer
            return None

        if len(end_buffer) > 0:
            # We had a silent phase but the user continued speaking
            # Feed the buffer with the silent phase and the current frame
            feed_buffer = np.append(end_buffer, vad_buffer)
            audio_buffer = np.append(audio_buffer, feed_buffer)
            end_buffer = np.array([], dtype=np.int16)
        else:
            # Feed current frame
            audio_buffer = np.append(audio_buffer, vad_buffer)

    else:
        # Keep frames for end padding or if user continues speaking after a short break
        end_buffer = np.append(end_buffer, vad_buffer)

    return None
