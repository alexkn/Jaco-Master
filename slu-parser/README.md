# Speech To Text Module

[DeepSpeech](https://github.com/mozilla/DeepSpeech) is used to convert the spoken commands to text.
This readme describes how to setup and debug the Speech To Text Module.

All commands are run from `Jaco-Master` directory.

## Setup

Build containers:

```bash
docker build -t slu_parser_amd64 - < slu-parser/Containerfile_amd64
docker build -t duckling_amd64 - < slu-parser/Containerfile_Duckling_amd64
```

Run the main script:

```bash
docker run --network host --rm \
  --volume `pwd`/slu-parser/:/Jaco-Master/slu-parser/:ro \
  --volume `pwd`/skills/generated/extras/:/Jaco-Master/slu-parser/moduldata/extras/:ro \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/jacolib/:ro \
  -it slu_parser_amd64 python3 /Jaco-Master/slu-parser/action-slu.py

docker run -p 8008:8008 --rm duckling_amd64
```

## Debugging

Connect to container:

```bash
docker run --network host --rm \
  --volume `pwd`/slu-parser/:/Jaco-Master/slu-parser/:ro \
  --volume `pwd`/slu-parser/moduldata/:/Jaco-Master/slu-parser/moduldata/ \
  --volume `pwd`/skills/generated/nlu/:/Jaco-Master/slu-parser/moduldata/nlu/:ro \
  --volume `pwd`/skills/generated/extras/:/Jaco-Master/slu-parser/moduldata/extras/:ro \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/jacolib/:ro \
  --volume ~/.asoundrc:/etc/asound.conf:ro \
  -it slu_parser_amd64
```

Build the SLU model:

```bash
# Run in container
python3 /Jaco-Master/slu-parser/build_model.py
```

Download a model checkpoint:

```bash
docker run --network host --rm \
  --volume `pwd`/slu-parser/:/Jaco-Master/slu-parser/:ro \
  --volume `pwd`/slu-parser/moduldata/graphdata/:/Jaco-Master/slu-parser/moduldata/graphdata/ \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/jacolib/:ro \
  -it slu_parser_amd64 \
    python3 /Jaco-Master/slu-parser/download_stt_model.py --language de
```

Test duckling: \
(Assumes duckling container running)

```bash
curl -X POST --data 'locale=en_US&text=tomorrow at eight' http://0.0.0.0:8008/parse
```

Test transcription over streamed topic: \
(Note that the first result might differ from the others if sending the same file multiple times,
because the pre-buffer is empty the first time and contains some zeros the following runs) \
(Assumes stt-parser and mqtt-broker already running) \
(Optionally run audio-streamer to test toggle audios)

```bash
# Run in container
# You have to record a file.wav with one of the above methods before

python3 /Jaco-Master/slu-parser/tests/test_streamed_wav.py \
  --path_wav /Jaco-Master/slu-parser/tests/data/riddle-answer.wav
```
