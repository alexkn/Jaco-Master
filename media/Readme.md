The image of the gray parrot in the logo is based upon:
https://commons.wikimedia.org/wiki/File:Psittacus_erithacus_-upper_body-8c.jpg

The snips architecture image is taken from:
https://jugsaxony.org/wp-content/uploads/2018/05/die-schoene-neue-welt-der-sprachassistenten.pdf
