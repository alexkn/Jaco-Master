import os
import time

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../../")

client = comm_tools.connect_mqtt_client(None, None)
satellite_name = utils.load_global_config()["satellite_names"][0].capitalize()

# Send wake word detected message
topic = "Jaco/WakeWord"
payload = {
    "data": True,
    "satellite": satellite_name,
    "timestamp": time.time(),
}
msg = comm_tools.encrypt_msg(payload, topic)
client.publish(topic, msg)
client.loop()
print("Sent wake word detected")

# Send intent detected message
topic = "Jaco/RecognizedIntent"
payload = {
    "intent": {"name": "skill_riddles-check_riddle", "confidence": 0.99},
    "text": "die loesung ist puppe",
    "entities": [{"entity": "skill_riddles-riddle_answers", "value": "puppe"}],
    "satellite": satellite_name,
    "timestamp": time.time(),
}
msg = comm_tools.encrypt_msg(payload, topic)
client.publish(topic, msg)
client.loop()
print("Sent intent detected")

# Send skill response message
topic = "Jaco/Skills/SayTextSkillRiddles"
payload = {
    "data": "Das war richtig",
    "satellite": satellite_name,
    "timestamp": time.time(),
}
msg = comm_tools.encrypt_msg(payload, topic)
client.publish(topic, msg)
client.loop()
print("Sent skill response")

# Send skill question message
topic = "Jaco/Skills/SayTextSkillRiddles"
question_intents = ["skill_dialogs-confirm", "skill_dialogs-refuse"]
payload = {
    "data": "Noch ein Rätsel?",
    "satellite": satellite_name,
    "question_intents": question_intents,
    "timestamp": time.time(),
}
msg = comm_tools.encrypt_msg(payload, topic)
client.publish(topic, msg)
client.loop()
print("Sent skill question")

# Send intent detected message
topic = "Jaco/RecognizedIntent"
payload = {
    "intent": {"name": "skill_dialogs-confirm", "confidence": 0.99},
    "text": "gerne doch",
    "satellite": satellite_name,
    "entities": [],
    "timestamp": time.time(),
}
msg = comm_tools.encrypt_msg(payload, topic)
client.publish(topic, msg)
client.loop()
print("Sent answer intent detected")

client.disconnect()
