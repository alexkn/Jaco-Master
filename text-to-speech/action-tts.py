import base64
import os
import subprocess

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

input_topic = "Jaco/SayText"
output_topic_base = "Jaco/{}/AudioWav"
language = utils.load_global_config()["language"]

cmd_base = '. /Jaco-Master/text-to-speech/speech_to_wav.sh "speech.wav" '
cmd_base += '"picotts" "{}" "--" "--" "--" "{}" "--" "--" "--"'


# ==================================================================================================


def on_connect(client):
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(client, userdata, msg):
    print("Received message on topic:", msg.topic)

    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        text = payload["data"]
        output_topic = output_topic_base.format(payload["satellite"].capitalize())

        print("Sending to {} the audio for the text: {}".format(output_topic, text))

        wav_content = create_and_read(text)
        wav_content = base64.b64encode(wav_content).decode()
        payload = {
            "data": wav_content,
            "timestamp": payload["timestamp"],
            "type": "speech",
        }
        msg_out = comm_tools.encrypt_msg(payload, output_topic)
        client.publish(output_topic, msg_out)

        print("Sent the audio to {}".format(output_topic))


# ==================================================================================================


def create_and_read(text):
    cmd = cmd_base.format(language, text)
    with subprocess.Popen(["/bin/bash", "-c", cmd]) as sp:
        sp.wait()

    with open("speech.wav", "rb") as file:
        content = file.read()

    cmd = "rm speech.wav"
    with subprocess.Popen(["/bin/bash", "-c", cmd]) as sp:
        sp.wait()

    return content


# ==================================================================================================


def main():
    client = comm_tools.connect_mqtt_client(on_connect, on_message)
    client.loop_forever()


# ==================================================================================================

if __name__ == "__main__":
    main()
