#! /bin/bash
set -e

ARCHITECTURE="arm64"

echo "Building duckling image ..."
docker buildx build --platform=linux/${ARCHITECTURE} --no-cache --squash -t duckling_${ARCHITECTURE} - < slu-parser/Containerfile_Duckling_${ARCHITECTURE}

echo "Building slu-parser image ..."
docker buildx build --platform=linux/${ARCHITECTURE} --no-cache --squash -t slu_parser_${ARCHITECTURE} - < slu-parser/Containerfile_${ARCHITECTURE}

echo ""
echo "FINISHED BUILD"
