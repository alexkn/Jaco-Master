#! /bin/bash
set -e

ARCHITECTURE="arm64"
NAMESPACE=${NAMESPACE:='jaco-assistant'}
BRANCH=${BRANCH:='master'}

echo "Login into gitlab registry:"
docker login registry.gitlab.com

echo "Pulling base image ..."
echo "docker pull registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/master_base_image_${ARCHITECTURE}"
docker pull "registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/master_base_image_${ARCHITECTURE}"
echo "docker tag registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/master_base_image_${ARCHITECTURE}" "master_base_image_${ARCHITECTURE}"
docker tag "registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/master_base_image_${ARCHITECTURE}" "master_base_image_${ARCHITECTURE}"

echo ""
echo "FINISHED PULL"
