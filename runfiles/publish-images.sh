#! /bin/bash

ARCHITECTURE="arm64"
NAMESPACE=${NAMESPACE:="jaco-assistant"}
BRANCH=${BRANCH:="master"}

# Login into registry first
echo "Login into gitlab registry:"
docker login registry.gitlab.com

# echo ""
# echo "Uploading master base image ..."
# LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/master_base_image_${ARCHITECTURE}"
# docker tag "master_base_image_${ARCHITECTURE}" "${LINK}"
# docker push "${LINK}"

# echo ""
# echo "Uploading mqtt-broker image ..."
# LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/mqtt_broker_${ARCHITECTURE}"
# docker tag "mqtt_broker_${ARCHITECTURE}" "${LINK}"
# docker push "${LINK}"

echo ""
echo "Uploading slu-parser image ..."
LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/slu_parser_${ARCHITECTURE}"
docker tag "slu_parser_${ARCHITECTURE}" "${LINK}"
docker push "${LINK}"

echo ""
echo "Uploading duckling image ..."
LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/duckling_${ARCHITECTURE}"
docker tag "duckling_${ARCHITECTURE}" "${LINK}"
docker push "${LINK}"

# echo ""
# echo "Uploading text-to-speech image ..."
# LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/${BRANCH}/text_to_speech_${ARCHITECTURE}"
# docker tag "text_to_speech_${ARCHITECTURE}" "${LINK}"
# docker push "${LINK}"

echo ""
echo "FINISHED UPLOADS"
