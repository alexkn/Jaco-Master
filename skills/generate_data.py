import itertools
import json
import os
import re

import sentence_tools
from jacolib import utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../")

nlu_collection_path = filepath + "collected/nlu/"
nlu_generation_path = filepath + "generated/nlu/"

intent_slot_path = filepath + "generated/extras/intent_slot_map.json"
with open(intent_slot_path, "r", encoding="utf-8") as mfile:
    intent_slot_map = json.load(mfile)


# ==================================================================================================


def clean_lookups(lookups: dict) -> dict:
    """Remove unnecessary synonym definitions and duplicate lookup values"""

    # Check for duplicates
    new_lookups = {}
    for lookup in lookups:
        new_lookups[lookup] = []

        # Replace synonym definitions if synonym and value are identical
        for sample in lookups[lookup]:
            if "->" in sample:
                val, syn = sample.split("->")
                vals = val[1:-1].split("|")
                if all(v == syn for v in vals):
                    new_lookups[lookup].append(syn)
                else:
                    # Rebuild lookup value to remove possible duplicates
                    new_value = "({})->{}".format("|".join(set(vals)), syn)
                    new_lookups[lookup].append(new_value)
            else:
                new_lookups[lookup].append(sample)

    # Remove duplicate values
    lookups = new_lookups
    new_lookups = {}
    for lookup in lookups:
        new_lookups[lookup] = sorted(set(lookups[lookup]))

    # Check that the same slot value doesn't occur as distinct value and in a synonym replacement
    duplicates = {}
    checked = {}
    for lookup in new_lookups:
        duplicates[lookup] = set()
        checked[lookup] = set()

        for sample in new_lookups[lookup]:
            if "->" in sample:
                val, syn = sample.split("->")
                vals = val[1:-1].split("|")
                vals.append(syn)
                svals = set(vals)
                for v in svals:
                    if v not in checked[lookup]:
                        checked[lookup].add(v)
                    else:
                        duplicates[lookup].add(v)
            else:
                if sample not in checked[lookup]:
                    checked[lookup].add(sample)
                else:
                    duplicates[lookup].add(sample)

    if sum([len(duplicates[lookup]) for lookup in duplicates]) > 0:
        for lookup in dict(duplicates):
            if len(duplicates[lookup]) == 0:
                duplicates.pop(lookup)
        msg = "Following lookup words occur as distinct value and in synonym replacements:"
        print("WARNING:", msg, str(duplicates))

    return new_lookups


# ==================================================================================================


def clean_intents(intents):
    """Extending nlu data with generated sentences too, because rasa doesn't detect
    entities from lookups if there are not enough example usages"""

    new_intents = {}
    for intent in intents:

        sents = []
        for sample in intents[intent]:
            # Remove entity example values
            sample = re.sub(r"\[[^\(]*\]", "[---]", sample)
            sents.append(sample)

        new_intents[intent] = sorted(set(sents))

    # Check that the same utterance doesn't occur in multiple intents
    duplicates = set()
    checked = set()
    for intent in new_intents:
        for sample in new_intents[intent]:
            if sample not in checked:
                checked.add(sample)
            else:
                duplicates.add(sample)
    if len(duplicates) > 0:
        msg = "Following sentences ({}/{}) occur in multiple intents: {}"
        print("WARNING:", msg.format(len(duplicates), len(checked), str(duplicates)))

    return new_intents


# ==================================================================================================


def main():

    nlu_files = os.listdir(nlu_collection_path)
    nlu_files = [f for f in nlu_files if f != ".gitignore"]
    print("Merging {} nlu data files".format(len(nlu_files)))

    lookups = sentence_tools.collect_lookups(nlu_collection_path, nlu_files)
    intents = sentence_tools.collect_list_examples(nlu_collection_path, nlu_files)

    # Drop unused intents
    found_intents = len(intents)
    intents = sentence_tools.drop_unused_intents(intents)
    dropped = found_intents - len(intents)
    print("Dropped {} unused intents".format(dropped))

    # Drop empty lookups
    found_lookups = len(lookups)
    for lookup in dict(lookups):
        if len(lookups[lookup]) == 0:
            lookups.pop(lookup)
    dropped = found_lookups - len(lookups)
    print("Dropped {} empty lookups".format(dropped))

    # Drop unused lookups
    found_lookups = len(lookups)
    used_entities = set(itertools.chain.from_iterable(intent_slot_map.values()))
    for lookup in dict(lookups):
        if lookup not in used_entities:
            lookups.pop(lookup)
    dropped = found_lookups - len(lookups)
    print("Dropped {} unused lookups".format(dropped))

    # Extend lookup alternatives
    new_lookups = {}
    for lookup in lookups:
        new_lookups[lookup] = []
        for sample in lookups[lookup]:
            new_values = sentence_tools.inject_lookup_alternatives(sample)
            new_lookups[lookup].extend(new_values)
    lookups = new_lookups

    # Remove unnecessary synonym definitions and duplicates
    found_lookups = sum([len(lookups[lookup]) for lookup in lookups])
    lookups = clean_lookups(lookups)
    dropped = found_lookups - sum([len(lookups[lookup]) for lookup in lookups])
    print("Dropped {} duplicate lookups".format(dropped))

    # Remove duplicates in intents and check for duplicates between intents
    found_intents = sum([len(intents[intent]) for intent in intents])
    intents = clean_intents(intents)
    dropped = found_intents - sum([len(intents[intent]) for intent in intents])
    print("Dropped {} duplicate intents".format(dropped))

    # Delete and recreate data directories
    utils.emtpy_with_ignore(nlu_generation_path)

    # Save generated nlu sentences
    nlu_data = {"intents": intents, "lookups": lookups}
    with open(nlu_generation_path + "nlu.json", "w+", encoding="utf-8") as file:
        json.dump(nlu_data, file, indent=2)


# ==================================================================================================

if __name__ == "__main__":
    main()
