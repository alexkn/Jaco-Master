import os
import sys

from jacolib import utils

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.append(filepath + "../")
import collect_data  # noqa: E402 pylint: disable=wrong-import-position
import sentence_tools  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
nlu_data_path = filepath + "data/skills/skills/testskill/nlu/"
nlu_collection_path = filepath + "data/skills/collected/nlu/"
utils.set_repo_path(filepath + "data/")

# ==================================================================================================


def test_collect() -> None:
    utils.emtpy_with_ignore(nlu_collection_path)

    # Collect data
    for file_name in os.listdir(nlu_data_path):
        path = nlu_data_path + file_name
        content = collect_data.process_file_content(path, "testskill")

        path = nlu_collection_path + "testskill" + "-" + file_name.lower()
        with open(path, "w+", encoding="utf-8") as file:
            file.write(content)

    # Check lookups
    nlu_files = os.listdir(nlu_collection_path)
    lookups = sentence_tools.collect_lookups(nlu_collection_path, nlu_files)
    sol = {"testskill-riddle_answers": ["fire", "(bike|ebike)->bike", "rainbow"]}
    assert sol == lookups

    # Check intents
    nlu_files = os.listdir(nlu_collection_path)
    intents = sentence_tools.collect_list_examples(nlu_collection_path, nlu_files)
    sol = {
        "testskill-get_riddle": ["i want a riddle", "a new riddle please"],
        "testskill-check_riddle": [
            "the answer is [snow](testskill-riddle_answers)",
            "is the solution [doll](testskill-riddle_answers?test)",
        ],
    }
    assert sol == intents


# ==================================================================================================


def test_drop_unused_intents() -> None:
    intents = {
        "testskill-get_riddle": "",
        "testskill-check_riddle": "",
        "skill_dialogs-refuse": "",
    }
    used_intents = sentence_tools.drop_unused_intents(intents)

    sol = {"testskill-get_riddle": "", "testskill-check_riddle": ""}
    assert sol == used_intents


# ==================================================================================================


def test_inject_lookup_alternatives() -> None:
    samples = [
        "tomorrow",
        "(the day after tomorrow)->in two days",
        "(this| ) (saturday|sunday)",
    ]

    new_samples = []
    for sample in samples:
        new_samples.extend(sentence_tools.inject_lookup_alternatives(sample))

    sol = [
        "tomorrow",
        "(the day after tomorrow)->in two days",
        "this saturday",
        "this sunday",
        "saturday",
        "sunday",
    ]
    assert set(sol) == set(new_samples)
