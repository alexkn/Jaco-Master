## intent:get_riddle
- i want a riddle!
- a new riddle please.

## lookup:riddle_answers
riddle_answers.txt

## intent:check_riddle
- the answer is: [snow](riddle_answers)
- is the solution [doll](riddle_answers?test)?
