import json
import os
import re

import sentence_tools
from jacolib import utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../")

nlu_data_path = filepath + "collected/nlu/"
extra_data_path = filepath + "generated/extras/"


# ==================================================================================================


def main():
    # Delete and recreate data directory
    utils.emtpy_with_ignore(extra_data_path)

    map_is = {}

    print("Collecting slots in intents ...")
    nlu_files = os.listdir(nlu_data_path)
    nlu_files = [f for f in nlu_files if f != ".gitignore"]
    print("Found {} nlu data files".format(len(nlu_files)))

    # Get intents and slots
    intents = sentence_tools.collect_list_examples(nlu_data_path, nlu_files)

    for intent in intents:
        map_is[intent] = set()

        for line in intents[intent]:
            matches = re.findall(r"]\(([a-zA-Z0-9?\-_]+)\)", line)
            if len(matches) > 0:
                entities = [re.sub(r"\?[a-zA-Z0-9_]+", "", match) for match in matches]
                map_is[intent] = map_is[intent].union(set(entities))

    for intent in map_is:
        map_is[intent] = sorted(list(map_is[intent]))

    # Save generated sentences to file
    with open(extra_data_path + "intent_slot_map.json", "w+", encoding="utf-8") as file:
        json.dump(map_is, file, indent=2)


# ==================================================================================================

if __name__ == "__main__":
    main()
