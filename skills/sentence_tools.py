import os
import re
from typing import List

from jacolib import comm_tools, extra_tools

# ==================================================================================================


def collect_lookups(nlu_data_path: str, nlu_files: list) -> dict:
    """Collect all lookup entity examples from the nlu files"""

    lookups = {}
    for file_name in nlu_files:
        name, extension = os.path.splitext(file_name)
        if extension != ".txt":
            continue

        with open(nlu_data_path + file_name, "r", encoding="utf-8") as file:
            content = file.readlines()
        content = [x.strip() for x in content]
        content = [x for x in content if x != ""]
        lookups[name] = content

    return lookups


# ==================================================================================================


def collect_list_examples(nlu_data_path: str, nlu_files: list) -> dict:
    """Collect a dictionary with all intents/synonyms and their example sentences/words"""

    prefix = "## intent:"
    intents = {}

    for file_name in nlu_files:
        # pylint: disable=W0612
        name, extension = os.path.splitext(file_name)
        if extension != ".md":
            continue

        with open(nlu_data_path + file_name, "r", encoding="utf-8") as file:
            content = file.readlines()
            content = [x.strip() for x in content]

        # Add empty line so that the end of the last intent will be detected
        content.append("")

        examples = []
        intent = ""
        at_intent = False
        for line in content:
            if line.startswith(prefix):
                at_intent = True
                intent = line.replace(prefix, "")

            elif at_intent and line.startswith("-"):
                line = line[2:]
                examples.append(line)

            else:
                if at_intent:
                    intents[intent] = list(examples)
                    examples = []
                at_intent = False

    return intents


# ==================================================================================================


def drop_unused_intents(intents: dict) -> dict:
    """Remove all intents which are not used by the skills (for example those from Skill-Dialogs).
    The function uses the topic keys for this."""

    topic_keys = comm_tools.load_topic_keys()
    used_intents = dict(intents)

    for intent in intents:
        topic = extra_tools.intent_to_topic(intent)
        if topic not in topic_keys:
            used_intents.pop(intent)

    return used_intents


# ==================================================================================================


def inject_lookup_alternatives(sentence: str) -> List[str]:
    """Create list of sentences with every possible alternative word combination"""

    lines = [sentence]
    alt_pattern = r"(?<!])\(([^)]+)\)(?!->)"
    matches = re.findall(alt_pattern, sentence)

    if len(matches) > 0:
        for block in matches:
            partial_lines = []
            for line in lines:
                for alt in block.split("|"):
                    sen = re.sub(alt_pattern, alt, line, count=1)
                    sen = re.sub(r"\s+", " ", sen)
                    partial_lines.append(sen)

            # If there are multiple alternatives in one sentence, go through the list again
            # This will result in exponential increase of the sentence amount
            lines = partial_lines

    lines = [line.strip() for line in lines]
    return lines
