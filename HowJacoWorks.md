# How Jaco works

This readme describes how the different modules of Jaco work together to create a voice assistant.

## Overview

<div align="center">
    <img src="media/jaco_communication.png" alt="jaco communication" width="75%"/>
</div>

In the above image there are two different _Satellites (Alpha, Beta)_ and the _Master_.
Both satellites have a microphone from which the _Audio Streamer_ reads the audio input
and streams it to the _Wake Word_ modules. If they detect the wake word in the stream,
they send a message to the _Dialog Manager_. \
After checking which Satellite detected the wake word first,
the _Dialog Manager_ sends an activation request to the _Speech to Text_ module.
The transcription of the speech is then started and continues until the user stops speaking.
Afterwards the detected text will be send to the _Nlu Parser_ which extracts the intent
and sends it back to the _Dialog Manager_. From there it will be send to the skill
which handles this intent type. \
The skill can can run some actions (like turning on the light in the lab)
and responds afterwards with a text message. The _Dialog Manager_ adds the Satellite which triggered the
wake word to the message and sends it to the _Text to Speech_ module. There the text will be converted
into an audio file which then will be send to the Satellite mentioned in the message.
The _Audio Streamer_ then plays the file on the speaker to the user.

## Communication

All module to module communication is handled with the MQTT-Protocol. A module can publish messages
to a topic (like `Jaco/RecognizedText`) or subscribe to topics. When a module publishes onto a topic,
all subscribing modules get notified. All the messages are distributed by a central MQTT-Broker.

<div align="center">
    <img src="media/jaco_topics.png" alt="jaco topics" width="75%"/>
</div>

In the above image you can see the most important topics Jaco uses. Note that these topics can be received
network wide, so you can access them from the skill too.

## Skills

Skills are similar to the modules from Jaco. They are container-based (that developers can provide you
with a tested environment and you don't have to install anything) and run actions. Additionally they
bring the intent examples with them on which the nlu and stt models are trained.

## Privacy

To keep your private things private, Jaco by default runs completely offline. Because everything is open source,
you (or a friend or some institution you trust) can check the source code. And of course you can change everything
to your special needs if you like.

An important idea why skills use containers, is to create something like a No-Trust-Environment. \
The skills should be able to use all resources of the host device, so that Jaco doesn't restrict the
developers needs and they can do what ever they like. But also the users (of skills from other developers)
should be always informed (by the skill's config file) what resources the skill can access
(somewhat similar to app permissions).

It's possible to build a skill that can listen directly to your microphone and create a
sound reactive room lightening system for example, but it's also ensured that a skill that says
it only tells jokes doesn't do the same thing. \
That's why the topics have an extra layer of encryption (encryption keys are only provided for topics
listed in the config file) and skills by default only have write access to their `skilldata` directory.

Another benefit of this concept is, that I (and other Jaco developers) don't have to check each skill
by hand in detail before adding them to the store.
